﻿using System;
using System.Collections.Generic;
using System.Text.Json;

namespace BattleShip.ConsoleUI
{
    class BattleShip
    {
        readonly static List<char> AcceptableLetters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
        const int XLength = 10;
        const int YLength = 10;
        const int FourdeckerCount = 1;
        const int ThreedeckerCount = 2;
        const int TwodeckerCount = 3;
        const int SingledeckerCount = 4;
        public void StartBattle()
        {
            Field field = new Field(XLength, YLength, FourdeckerCount, ThreedeckerCount, TwodeckerCount, SingledeckerCount);
            Console.WriteLine("Generated new field.");
            bool isGameFinished = false;
            while (!isGameFinished)
            {
                Console.WriteLine("Enter coords of target (Letter in [ABCDEFGHIJ] and number in [1-10]):");
                string input = Console.ReadLine();
                try
                {
                    ParseCoords(input, field);
                    if (field.Ships.TrueForAll(x => x.IsSunked))
                    {
                        Console.WriteLine("\n\nYou won.\n\n");
                        isGameFinished = true;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            StartBattle();
        }
        static void ParseCoords(string input, Field field)
        {
            if (input == "json")
            {
                ShowShipsJSON(field);
            }
            if (input == "field")
            {
                ShowField(field);
            }
            if (input.Length < 2 || input.Length > 3)
            {
                throw new ArgumentException("Wrong coords!");
            }
            if (!int.TryParse(input.Remove(0, 1), out int coordinateY))
            {
                throw new ArgumentException("Wrong coords!");
            }
            if (coordinateY > YLength || coordinateY < 0)
            {
                throw new ArgumentException("Wrong coords!");
            }
            input.ToUpper();
            if (!AcceptableLetters.Contains(input[0]))
            {
                throw new ArgumentException("Wrong coords!");
            }
            Console.WriteLine(field.Hit(AcceptableLetters.IndexOf(input[0]), coordinateY - 1));
        }
        static void ShowField(Field field)
        {
            for (int i = 0; i < YLength; i++)
            {
                for (int j = 0; j < XLength; j++)
                {
                    if (field.GetCoordinate(j, i).IsFilled)
                    {
                        Console.Write(" z");
                    }
                    else
                    {
                        Console.Write(" .");
                    }
                }
                Console.WriteLine();
            }
        }
        static void ShowShipsJSON(Field field)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            foreach (var ship in field.Ships)
            {
                Console.WriteLine(JsonSerializer.Serialize(ship, options));
            }
        }
    }
}
