﻿namespace BattleShip.ConsoleUI
{
    class Program
    {
        static void Main()
        {
            BattleShip battleShip = new BattleShip();
            battleShip.StartBattle();
        }    
    }
}
