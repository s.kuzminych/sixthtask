using System.Collections.Generic;
using NUnit.Framework;

namespace BattleShip.Tests
{
    public class BattleShipTests
    {
        [Test]
        public void Hit_X3Y6_CorrectStatusReturned()
        {
            int coordinateX = 3;
            int coordinateY = 6;
            
            Field field = new Field(10,10,1,2,3,4);
            string actualResult = field.Hit(coordinateX, coordinateY);
            
            string expectedResult;

            if (!field.Coordinates.Find(x => x.X == 3 && x.Y==6).IsFilled)
            {
                expectedResult = "Missed.";
            }
            else if (field.GetShip(field.GetCoordinate(coordinateX, coordinateY)).Coordinates.Count==1)
            {
                expectedResult = "Sunked.";
            }
            else
            {
                expectedResult = "Hited.";
            }
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}