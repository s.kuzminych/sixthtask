﻿using System;
using System.Collections.Generic;

namespace BattleShip
{
    [Serializable]
    public abstract class Ship
    {
        public string Name { get; set; }
        public int Length { get; set; }
        public List<Coordinate> Coordinates { get; set; }
        public List<Coordinate> HitedCoordinates { get; set; }
        public bool IsHited
        { 
            get
            {
                return this.HitedCoordinates.Count > 0;
            }
        }
        public bool IsSunked
        {
            get
            {
                return this.Coordinates.Count == this.HitedCoordinates.Count;
            }
        }
        public Ship(List<Coordinate> coordinates)
        {
            Coordinates = coordinates;
            HitedCoordinates = new List<Coordinate>();
        }
    }
    [Serializable]
    public class FourDecker : Ship
    {
        public FourDecker(List<Coordinate> coordinates, int number) : base(coordinates)
        {
            Name = "Battleship#" + number.ToString();
            Length = 4;
            
        }
    }
    [Serializable]
    public class ThreeDecker : Ship
    {
        public ThreeDecker(List<Coordinate> coordinates, int number) : base(coordinates)
        {
            Name = "Cruiser#" + number.ToString();
            Length = 3;
        }
    }
    [Serializable]
    public class TwoDecker : Ship
    {
        public TwoDecker(List<Coordinate> coordinates, int number) : base(coordinates)
        {
            Name = "Destroyer#" + number.ToString();
            Length = 2;
        }
    }
    [Serializable]
    public class SingleDecker : Ship
    {
        public SingleDecker(List<Coordinate> coordinates, int number) : base(coordinates)
        {
            Name = "Submarine#" + number.ToString();
            Length = 1;
        }
    }
}
