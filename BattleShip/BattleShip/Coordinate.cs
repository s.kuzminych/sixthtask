﻿using System;
using System.Text.Json.Serialization;

namespace BattleShip
{
    [Serializable]
    public class Coordinate
    {
        public int X { get; set; }
        public int Y { get; set; }
        [JsonIgnore]
        public bool IsFilled { get; set; }
        [JsonIgnore]
        public bool IsHited { get; set; }
        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
            IsFilled = false;
            IsHited = false;
        }
    }
}
