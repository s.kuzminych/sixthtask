﻿using System;
using System.Collections.Generic;

namespace BattleShip
{
    public class Field
    {
        
        static int XLength;
        static int YLength;
        public List<Coordinate> Coordinates = new List<Coordinate>();
        public List<Ship> Ships = new List<Ship>();
        public Field(int xLength, int yLength, int fourdeckerCount, int threedeckerCount, int twodeckerCount, int singledeckerCount)
        {
            XLength = xLength;
            YLength = yLength;
            for (int x = 0; x < xLength; x++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    Coordinates.Add(new Coordinate(x,y));
                }
            }
            for (int i = 0; i < fourdeckerCount; i++)
            {
                Ships.Add(new FourDecker(GetNewPlaceForShip(4), i + 1));
            }
            for (int i = 0; i < threedeckerCount; i++)
            {
                Ships.Add(new ThreeDecker(GetNewPlaceForShip(3), i + 1));
            }
            for (int i = 0; i < twodeckerCount; i++)
            {
                Ships.Add(new TwoDecker(GetNewPlaceForShip(2), i + 1));
            }
            for (int i = 0; i < singledeckerCount; i++)
            {
                Ships.Add(new SingleDecker(GetNewPlaceForShip(1), i + 1));
            }
        }
        private static readonly Random Random = new Random();
        public static int GetRandomNumber(int min, int max)
        {
            lock(Random)
            {
                return Random.Next(min, max);
            }
        }
        public List<Coordinate> GetNewPlaceForShip(int lengthOfShip)
        {
            
            while (true)
            {
                int coordinateIndex = GetRandomNumber(0,Coordinates.Count);
                Coordinate suggestedCoordinate = Coordinates[coordinateIndex];
                int suggestedVerticalDirection = GetRandomNumber(0, 2);
                int suggestedHorisontalDirection = 1 - suggestedVerticalDirection;
                int suggestedMinusDirection = GetRandomNumber(0, 2);
                int suggestedPlusDirection = 1 - suggestedMinusDirection;
                List<Coordinate> coordinates = new List<Coordinate>();
                for (int i = 0; i < lengthOfShip; i++)
                {
                    suggestedCoordinate = GetCoordinate(suggestedCoordinate.X + suggestedHorisontalDirection * suggestedPlusDirection - suggestedHorisontalDirection * suggestedMinusDirection, suggestedCoordinate.Y + suggestedVerticalDirection * suggestedPlusDirection - suggestedVerticalDirection * suggestedMinusDirection);
                    if (suggestedCoordinate == null)
                    {
                        break;
                    }
                    if (CheckForNeighbourCoords(suggestedCoordinate))
                    {
                        break;
                    }
                    coordinates.Add(suggestedCoordinate);
                }
                if (coordinates.Count < lengthOfShip)
                {
                    continue;
                }
                foreach (var coordinate in coordinates)
                {
                    Coordinates.Find(x => x == GetCoordinate(coordinate.X, coordinate.Y)).IsFilled = true;
                }
                return coordinates;
            }
        }

        public bool CheckForNeighbourCoords(Coordinate coordinate)
        {
            int coordinateX = coordinate.X;
            int coordinateY = coordinate.Y;
            int startCoordinateX = coordinateX - 1;
            int finalCoordinateX = coordinateX + 1;
            int startCoordinateY = coordinateY - 1;
            int finalCoordinateY = coordinateY + 1;
            if (coordinateX == 0)
            {
                startCoordinateX = coordinateX;
            }
            else if (coordinateX == XLength - 1)
            {
                finalCoordinateX = coordinateX;
            }
            if (coordinateY == 0)
            {
                startCoordinateY = coordinateY;
            }
            else if (coordinateY == YLength - 1)
            {
                finalCoordinateY = coordinateY;
            }
            for (int i = startCoordinateX; i <= finalCoordinateX; i++)
            {
                for (int j = startCoordinateY; j <= finalCoordinateY; j++)
                {
                    if (GetCoordinate(i,j).IsFilled)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public Coordinate GetCoordinate(int coordinateX, int coordinateY)
        {
            return Coordinates.Find(x => x.X == coordinateX && x.Y == coordinateY);
        }
        public Ship GetShip(Coordinate coordinate)
        {
            return Ships.Find(x => x.Coordinates.Contains(coordinate));
        }
        public string Hit(int coordinateX, int coordinateY)
        {
            Coordinate coordinate = GetCoordinate(coordinateX, coordinateY);
            if (coordinate.IsHited)
            {
                return "Already hited.";
            }
            else if (!coordinate.IsFilled)
            {
                Coordinates.Find(x => x == coordinate).IsHited = true;
                return "Missed.";
            }
            else
            {
                Coordinates.Find(x => x == coordinate).IsHited = true;
                Ship ship = GetShip(coordinate);
                ship.HitedCoordinates.Add(coordinate);
                if (ship.IsSunked)
                {
                    return "Sunked."; 
                }
                else
                {
                    return "Hited.";
                }
            }
        }
    }
}
